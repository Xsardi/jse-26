package ru.t1.tbobkov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.tbobkov.tm.api.component.ISaltProvider;

public interface IPropertyService extends ISaltProvider {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getApplicationConfig();

    @NotNull
    String getApplicationName();

    @NotNull
    String getApplicationLog();

    @NotNull
    String getAuthorEmail();

    @NotNull
    String getAuthorName();

    @NotNull
    String getGitBranch();

    @NotNull
    String getGitCommitId();

    @NotNull
    String getGitCommitterName();

    @NotNull
    String getGitCommitterEmail();

    @NotNull
    String getGitCommitMessage();

    @NotNull
    String getGitCommitTime();

}
