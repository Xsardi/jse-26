package ru.t1.tbobkov.tm.exception.system;

public final class PermissionException extends AbstractSystemException {

    public PermissionException() {
        super("Error! User doesn't have required permissions...");
    }

}

