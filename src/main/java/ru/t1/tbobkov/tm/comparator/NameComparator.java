package ru.t1.tbobkov.tm.comparator;

import org.jetbrains.annotations.Nullable;
import ru.t1.tbobkov.tm.api.model.IHasName;

import java.util.Comparator;

public enum NameComparator implements Comparator<IHasName> {

    INSTANCE;

    @Override
    public int compare(@Nullable IHasName o1, @Nullable IHasName o2) {
        if (o1 == null || o2 == null) return 0;
        return o1.getName().compareTo(o2.getName());
    }

}
