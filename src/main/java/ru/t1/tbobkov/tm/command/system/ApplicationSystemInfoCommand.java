package ru.t1.tbobkov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.t1.tbobkov.tm.enumerated.Role;

import static ru.t1.tbobkov.tm.util.FormatUtil.formatBytes;

public final class ApplicationSystemInfoCommand extends AbstractSystemCommand {

    @NotNull
    public static final String ARGUMENT = "-i";

    @NotNull
    public static final String DESCRIPTION = "show system info";

    @NotNull
    public static final String NAME = "info";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[SYSTEM INFO]");
        final int processorsCount = Runtime.getRuntime().availableProcessors();
        System.out.println("PROCESSORS: " + processorsCount);

        final long maxMemory = Runtime.getRuntime().maxMemory();
        final long totalMemory = Runtime.getRuntime().totalMemory();
        final long freeMemory = Runtime.getRuntime().freeMemory();
        final long usedMemory = totalMemory - freeMemory;
        System.out.println("MAXIMUM MEMORY: " + formatBytes(maxMemory));
        System.out.println("TOTAL MEMORY: " + formatBytes(totalMemory));
        System.out.println("FREE MEMORY: " + formatBytes(freeMemory));
        System.out.println("USED MEMORY: " + formatBytes(usedMemory));
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
